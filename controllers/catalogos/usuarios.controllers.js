const usuarios                = require("../../models/catalogos/usuarios.model.js");
const configuracion_terapeuta = require("../../models/citas/configuracion_terapeuta.model.js");

const config           = require("../../config/index.js");
const md5              = require('md5');
const { v4: uuidv4 }   = require('uuid')


// AGREGAR USUARIO
exports.addUsuario = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR EL USUARIO
    const usuarioGenerado  = await usuarios.addUsuario( req.body ).then( response => response )

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Usuario generado correctamente', usuarioGenerado })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// AGREGAR LA FOTO AL USUARIO
exports.addFotoUsuario = async ( req, res) => {
  try{
    
    // desestrucutramos los arvhios cargados
    const { file } = req.files

    // Si no tiene archivo se debe agregar como quiera
    if( !file ){
      return res.status( 400 ).send({ message : 'El contenido no puede ir vacio' })
    }

    const nombreSplit = file.name.split('.')
    
    // Obtener la extensión del archivo 
    const extensioArchivo = nombreSplit[ nombreSplit.length - 1 ]
    
    // modificar el nombre del archivo con un uuid unico
    const nombreUuid = uuidv4() + '.' + extensioArchivo
    
    // extensiones validas
    let extensiones  = [ 'BMP', 'GIF', 'jpg', 'jpeg', 'png', 'webp', 'PNG', 'JPG', 'JPEG', 'gif', 'bmp' ]
    let ruta         = './../../hope-imagenes/' + nombreUuid

    // validar que la extensión del archivo recibido sea valido
    if( !extensiones.includes( extensioArchivo) )
      return res.status( 400 ).send({message: `Hola, la estensión: ${ extensioArchivo } no es valida, solo se aceptan: ${ extensiones }`})
    

    file.mv(`${ruta}`, err => {
      if( err )
        return res.status( 400 ).send({ message: err })
      
      return res.send({ message: 'Imagen cargada correctamente', nombre: nombreUuid, extension: extensioArchivo })
    })

  }catch( error ){
    res.status( 500 ).send({ message: error ? error.message : 'Error en el servidor' })
  }

}

// OBTENER TODAS LOS USUARIOS
exports.getUsuarios = async (req, res) => {
  try{
    
    // OBTENER TODAS LOS USUARIOS MENOS LOS ELIMINADOS
    const usuariosAll  = await usuarios.getUsuarios( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( usuariosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

exports.getTerapeutas = async (req, res) => {
  try{
    
    // OBTENER TODAS LOS USUARIOS MENOS LOS ELIMINADOS
    const usuariosAll  = await usuarios.getTerapeutas( ).then( response => response )

    let idusuarios = usuariosAll.map(( registro ) => { return registro.idusuarios })

    idusuarios = idusuarios.length ? idusuarios : [0]

    const horarios  = await configuracion_terapeuta.getHorariosTerapeutas( idusuarios ).then( response => response )

    for( const i in usuariosAll ){
      const { idusuarios } = usuariosAll[i]

      usuariosAll[i]['horarios'] = horarios.filter( el => { return el.id_usuario == idusuarios })

    }

    // RESPUESTA AL USUARIO
    res.send( usuariosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


exports.getPacientes = async (req, res) => {
  try{
    
    // OBTENER TODAS LOS USUARIOS MENOS LOS ELIMINADOS
    const pacientesAll  = await usuarios.getPacientes( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( pacientesAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR EL USUARIO O BIEN ELIMINARLO
exports.updateUsuario = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // una vez enviado, hay que actualizar la contraseñ
    const usuarioActualizado = await usuarios.updateUsuario( req.body, id ).then( response=> response )

    res.send({ message: 'Usuario actualizado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER SOLO LOS USUARIOS ACTIVOS
exports.getUsuariosActivos = async (req, res) => {
  try{

    // OBTENER SOLO LOS USUARIOS ACTIVOS MENOS LOS ELIMINADOS
    const usuariosAll  = await usuarios.getUsuariosActivos( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( usuariosAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


exports.validarAcceso = async (req, res) => {
  try{
    // Desestrucutramos los datos
    const { correo, password } = req.body

    // Consultamos los datos del correo
    const existeUsuario = await usuarios.existeUsuario( correo ).then( response => response )

    // Si el usuario no existe, hay que retornar que no existe
    if(!existeUsuario){ return res.status( 400 ).send( { message: 'Usuario no existe' } ) }

    // Validar las contraseñas
    const usuario = await usuarios.validarPassword( correo, md5( password ) ).then( response => response )

    // Si el usuario no existe, hay que retornar que no existe
    if(!usuario){ return res.status( 400 ).send( { message: 'Contraseña incorrecta' } ) }

    res.send( usuario )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor' } )
  }
};
