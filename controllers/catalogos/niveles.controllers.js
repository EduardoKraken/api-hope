const niveles   = require("../../models/catalogos/niveles.model.js");
const config     = require("../../config/index.js");
const md5        = require('md5');

// AGREGAR NIVEL
exports.addNivel = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR EL NIVEL
    const nivelAgregado  = await niveles.addNivel( req.body ).then( response => response )

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Nivel generado correctamente', nivelAgregado })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODOS LOS NIVELES
exports.getNiveles = async (req, res) => {
  try{
    
    // OBTENER TODOS LOS NIVELES MENOS LOS ELIMINADOS
    const nivelesAll  = await niveles.getNiveles( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( nivelesAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR EL NIVEL O BIEN ELIMINARLO
exports.updateNivel = async (req, res) => {
  try{
    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // una vez enviado, hay que actualizar la contraseñ
    const nivelActualizado = await niveles.updateNivel( req.body, id ).then( response=> response )

    res.send({ message: 'Nivel actualizado correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER SOLO LOS NIVELES ACTIVOS
exports.getNivelesActivos = async (req, res) => {
  try{

    // OBTENER TODOS LOS NIVELES MENOS LOS ELIMINADOS
    const nivelesAll  = await niveles.getNivelesActivos( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( nivelesAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};
