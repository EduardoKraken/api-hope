const coordinaciones   = require("../../models/catalogos/coordinaciones.model.js");
const config     = require("../../config/index.js");
const md5        = require('md5');

// AGREGAR COORDINACION
exports.addCoordinacion = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    // AGREGAR EL COORDINACION
    const CoordinacionAgregada  = await coordinaciones.addCoordinacion( req.body ).then( response => response )

    // MENSAJE DE RESPUESTA AL USUARIO
    res.send({ message: 'Coordinación generada correctamente', CoordinacionAgregada })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODAS LAS COORDINACIONES
exports.getCoordinaciones = async (req, res) => {
  try{
    
    // OBTENER TODAS LAS COORDINACIONES MENOS LAS ELIMINADAS
    const coordinacionesAll  = await coordinaciones.getCoordinaciones( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( coordinacionesAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR LA COORDINACION O BIEN ELIMINARLA
exports.updateCoordinacion = async (req, res) => {
  try{
    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // una vez enviado, hay que actualizar la contraseñ
    const coordinacionActualizada = await coordinaciones.updateCoordinacion( req.body, id ).then( response=> response )

    res.send({ message: 'Coordinación actualizada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER SOLO LOS COORDINACIONES ACTIVAS
exports.getCoordinacionesActivas = async (req, res) => {
  try{

    // OBTENER TODOS LOS COORDINACIONES MENOS LOS ELIMINADOS
    const coordinacionesAll  = await coordinaciones.getCoordinacionesActivas( ).then( response => response )

    // RESPUESTA AL USUARIO
    res.send( coordinacionesAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};
