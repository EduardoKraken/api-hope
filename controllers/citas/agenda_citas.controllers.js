const agenda_citas    = require("../../models/citas/agenda_citas.model.js");
const historial_cita  = require("../../models/citas/historial_cita.model.js");
const evidencia_cita  = require("../../models/citas/evidencia_cita.model.js");
const config          = require("../../config/index.js");
const md5             = require('md5');
const { v4: uuidv4 }  = require('uuid')


// AGENDAR UNA CITA
exports.addAgendaCita = async (req, res) => {
  try{

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }
    
    const { id_terapeuta, id_paciente, fecha_cita, hora_inicio, hora_final } = req.body
    
    // COMPRAR SI AL PACIENTE NO SE LE EMPALMAN LAS FECHAS
    console.log( id_paciente, fecha_cita, hora_inicio, hora_final )
    const agendaPaciente = await agenda_citas.agendaPaciente( id_paciente, fecha_cita, hora_inicio, hora_final ).then( response => response )
    
    // VALIDAR INFORMACION
    if( agendaPaciente ){ return res.status( 400 ).send({ message: 'Se empalma fecha y hora del paciente' });  }


    // COMPRAR SI AL PACIENTE NO SE LE EMPALMAN LAS FECHAS
    const agendaTerapeuta = await agenda_citas.agendaTerapeuta( id_terapeuta, fecha_cita, hora_inicio, hora_final ).then( response => response )
    
    // VALIDAR INFORMACION
    if( agendaTerapeuta ){ return res.status( 400 ).send({ message: 'Se empalma fecha y hora del terapeuta' });  }

    // AGENDAR LA CITA
    const agendaGenerada  = await agenda_citas.addAgendaCita( req.body ).then( response => response )

    // MENSAJE DE RESPUESTA AL Agenda
    res.send({ message: 'Agenda generada correctamente', agendaGenerada })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
}

// OBTENER TODAS LAS CITAS EN GENERAL
exports.getAgendaCitas = async (req, res) => {
  try{
    
    // OBTENER LA AGENDA DE TODOS 
    const agendasAll  = await agenda_citas.getAgendaCitas( ).then( response => response )

    // CONSULTAR HISTORIAL DE LA AGENDA
    let idAgenda = agendasAll.map((registro)=> { return registro.idagenda_citas })
    idAgenda = idAgenda.length ? idAgenda : [0]

    // CONSULTAR EL HISTORIAL 
    const historialAgenda = await historial_cita.getHistorialCita( idAgenda ).then( response => response )

    // CONSULTAR LAS EVIDENCIAS
    const evidenciaCita   = await evidencia_cita.getEvidenciaCitas( idAgenda ).then( response => response )


    for( const i in agendasAll ){
      const { idagenda_citas } = agendasAll[i]

      agendasAll[i]['historial']  = historialAgenda.filter( el => { return el.idagenda_cita == idagenda_citas })
      agendasAll[i]['evidencias'] = evidenciaCita.filter( el => { return el.idagenda_cita == idagenda_citas })
    }

    // RESPUESTA AL Agenda
    res.send( agendasAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// ACTUALIZAR O DAR DE BAJA UNA CITA
exports.updateAgendaCita = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params

    // vALIDACIÓN DE QUE TODOS LOS DATOS HAYAN LLEGADO
    if (!req.body || Object.keys(req.body).length === 0 || !id ) {
      return res.status( 400 ).send({ message: 'El Contenido no puede estar vacio' });
    }

    // ACTUALIZAR LA AGENDA
    const agendaActualizada = await agenda_citas.updateAgendaCita( req.body, id ).then( response=> response )

    res.send({ message: 'Agenda actualizada correctamente' })

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

// OBTENER TODAS LAS CITAS AGENDADAS ACTIVAS
exports.getAgendasCitasActivas = async (req, res) => {
  try{

    // OBTENER SOLO LAS AGENDAS ACTIVAS MENOS LOS ELIMINADOS
    const agendasAll  = await agenda_citas.getAgendasCitasActivas( ).then( response => response )

    // CONSULTAR HISTORIAL DE LA AGENDA
    let idAgenda = agendasAll.map((registro)=> { return registro.idagenda_citas })
    idAgenda = idAgenda.length ? idAgenda : [0]

    // CONSULTAR EL HISTORIAL 
    const historialAgenda = await historial_cita.getHistorialCita( idAgenda ).then( response => response )

    // CONSULTAR LAS EVIDENCIAS
    const evidenciaCita   = await evidencia_cita.getEvidenciaCitas( idAgenda ).then( response => response )

    for( const i in agendasAll ){
      const { idagenda_citas } = agendasAll[i]

      agendasAll[i]['historial']  = historialAgenda.filter( el => { return el.idagenda_cita == idagenda_citas })
      agendasAll[i]['evidencias'] = evidenciaCita.filter( el => { return el.idagenda_cita == idagenda_citas })
    }

    // RESPUESTA AL Agenda
    res.send( agendasAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};


// OBTENER LA AGENDA DE UN USUARIO
exports.getAgendaCitasUsuario = async (req, res) => {
  try{

    // Desestrucutramos los datos
    const { id } = req.params
    
    // OBTENER LA AGENDA DE UN USUARIO
    const agendasAll  = await agenda_citas.getAgendaCitasUsuario( id ).then( response => response )

    // CONSULTAR HISTORIAL DE LA AGENDA
    let idAgenda = agendasAll.map((registro)=> { return registro.idagenda_citas })
    idAgenda = idAgenda.length ? idAgenda : [0]

    // CONSULTAR EL HISTORIAL 
    const historialAgenda = await historial_cita.getHistorialCita( idAgenda ).then( response => response )

    // CONSULTAR LAS EVIDENCIAS
    const evidenciaCita   = await evidencia_cita.getEvidenciaCitas( idAgenda ).then( response => response )

    for( const i in agendasAll ){
      const { idagenda_citas } = agendasAll[i]

      agendasAll[i]['historial']  = historialAgenda.filter( el => { return el.idagenda_cita == idagenda_citas })
      agendasAll[i]['evidencias'] = evidenciaCita.filter( el => { return el.idagenda_cita == idagenda_citas })
    }


    // RESPUESTA AL Agenda
    res.send( agendasAll )

  }catch( error ){
    res.status( 500 ).send( { message: error ? error.message : 'Error en el servidor, comunícate con sistemas' } )
  }
};

