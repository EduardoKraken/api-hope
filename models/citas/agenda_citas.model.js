const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const agenda_citas = (agenda_citas) => {};

// Agregar datos usuario
agenda_citas.addAgendaCita = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO agenda_citas( id_terapeuta, id_paciente, fecha_cita, hora_inicio, hora_final, idcoordinacion ) VALUES( ?, ?, ?, ?, ?, ? )`,
      [ c.id_terapeuta, c.id_paciente, c.fecha_cita, c.hora_inicio, c.hora_final, c.idcoordinacion ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};


agenda_citas.getAgendaCitas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas WHERE deleted = 0 AND estatus > 0;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

agenda_citas.updateAgendaCita = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE agenda_citas SET id_terapeuta= ?, id_paciente= ?, fecha_cita= ?, hora_inicio= ?, hora_final= ?, 
    	idcoordinacion= ?, estatus = ?, deleted = ? 
      WHERE idagenda_citas = ?`,
      [ c.id_terapeuta, c.id_paciente, c.fecha_cita, c.hora_inicio,c. hora_final, c.idcoordinacion, c.estatus, c.deleted, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    })
  })
};

agenda_citas.getAgendasCitasActivas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas WHERE deleted = 0 AND estatus > 0;`,(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};


agenda_citas.getAgendaCitasUsuario = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT a.id_terapeuta, a.id_paciente, a.fecha_cita, a.hora_inicio, a.hora_final, a.idcoordinacion,
			CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS paciente,
			CONCAT(u2.nombres, " ", u2.apellido_paterno, " ", u2.apellido_materno) AS terapeuta, a.idagenda_citas,
			c.color, c.coordinacion, a.estatus FROM agenda_citas a
			LEFT  JOIN coordinaciones c ON c.idcoordinaciones = a.idcoordinacion
			LEFT JOIN usuarios u ON u.idusuarios = a.id_paciente
			LEFT JOIN usuarios u2 ON u2.idusuarios = a.id_terapeuta
			WHERE u.idusuarios = ?
      ORDER BY a.fecha_cita DESC;`,[ id_usuario ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

agenda_citas.agendaPaciente = ( id_paciente, fecha_cita, hora_inicio, hora_final ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas
      WHERE id_paciente = ?
      AND fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final
      AND id_paciente = ?
      OR fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final;`,[ id_paciente, fecha_cita, hora_inicio, id_paciente, fecha_cita, hora_final ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  })
};

agenda_citas.agendaTerapeuta = ( id_terapeuta, fecha_cita, hora_inicio, hora_final ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM agenda_citas
      WHERE id_terapeuta = ?
      AND fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final
      AND id_terapeuta = ?
      OR fecha_cita = ?
      AND TIME(?) BETWEEN hora_inicio AND hora_final;`,[ id_terapeuta, fecha_cita, hora_inicio, id_terapeuta, fecha_cita, hora_final ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  })
};


agenda_citas.citaCumplida = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE agenda_citas SET estatus = 2 WHERE idagenda_citas = ?`, [ id ],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id  });
    })
  })
};

module.exports = agenda_citas;