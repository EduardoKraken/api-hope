const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const historial_cita = (historial_cita) => {};

// Agregar datos usuario
historial_cita.addHistorialCita = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO historial_cita( idagenda_cita, notas ) VALUES( ?, ? )`,
      [ c.idagenda_cita, c.notas ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};

historial_cita.updateHistorialCita = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE historial_cita SET notas = ?, estatus = ?, deleted = ? WHERE idhistorial_cita = ?`,
      [ c.notas, c.estatus, c.deleted, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    })
  })
};

historial_cita.getHistorialCita = ( id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM historial_cita WHERE deleted = 0 AND estatus > 0 AND idagenda_cita IN ( ? );`,
      [ id ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};


module.exports = historial_cita;