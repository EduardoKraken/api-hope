const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const horarios = (horarios) => {};

// Agregar datos usuario
horarios.addConfiguracionTerapeuta = ( id_usuario, dia, hora_inicio, hora_fin ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO configuracion_terapeuta( id_usuario, dia, hora_inicio, hora_fin ) VALUES( ?, ?, ?, ? )`,
      [ id_usuario, dia, hora_inicio, hora_fin ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, id_usuario, dia, hora_inicio, hora_fin });
    });
  })
};

horarios.existeHorario = ( id_usuario, dia ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM configuracion_terapeuta WHERE deleted = 0 AND estatus = 1 AND id_usuario = ? AND dia = ?;`,
      [id_usuario, dia ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res[0] );
    });
  })
};

horarios.getConfiguracionTerapeuta = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM configuracion_terapeuta WHERE deleted = 0 AND estatus = 1;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

horarios.updateConfiguracionTerapeuta = ( dia, hora_inicio, hora_fin, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE configuracion_terapeuta SET dia = ?, hora_inicio = ?, hora_fin = ?
      WHERE idconfiguracion_terapeuta = ?`, [ dia, hora_inicio, hora_fin, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, dia, hora_inicio, hora_fin, id });
    })
  })
};

horarios.getHorariosTerapeuta = ( id_usuario ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM configuracion_terapeuta WHERE deleted = 0 AND estatus = 1 AND id_usuario = ?;`,
      [ id_usuario ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

horarios.getHorariosTerapeutas = ( id_usuarios ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT *, (ELT(dia + 1,'Domingo' ,'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado','Domingo')) AS diaNombre FROM configuracion_terapeuta WHERE deleted = 0 AND estatus = 1 AND id_usuario IN (?);`,
      [ id_usuarios ],(err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

module.exports = horarios;