const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const niveles = (niveles) => {};

// Agregar datos usuario
niveles.addNivel = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO niveles( nivel ) VALUES( ? )`, [c.nivel], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};


niveles.getNiveles = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM niveles WHERE deleted = 0 ORDER BY nivel;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

niveles.updateNivel = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE niveles SET nivel = ?, estatus = ?, deleted = ? WHERE idniveles = ?`, [ c.nivel, c.estatus, c.deleted, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    })
  })
};

niveles.getNivelesActivos = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM niveles WHERE deleted = 0 AND estatus = 1 ORDER BY nivel;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

module.exports = niveles;