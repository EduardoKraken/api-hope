const { result } = require("lodash");
const sqlERP     = require("../db.js");


//const constructor
const coordinaciones = (coordinaciones) => {};

// Agregar datos usuario
coordinaciones.addCoordinacion = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO coordinaciones( coordinacion, color, correo ) VALUES( ?, ?, ? )`, [ c.coordinacion, c.color, c.correo], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};


coordinaciones.getCoordinaciones = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM coordinaciones WHERE deleted = 0 ORDER BY coordinacion;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

coordinaciones.updateCoordinacion = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE coordinaciones SET coordinacion = ?, color = ?, correo = ?, estatus = ?, deleted = ? WHERE idcoordinaciones = ?`, 
      [ c.coordinacion, c.color, c.correo, c.estatus, c.deleted, id],
      (err, res) => {
      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    })
  })
};

coordinaciones.getCoordinacionesActivas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM coordinaciones WHERE deleted = 0 AND estatus = 1 ORDER BY coordinacion;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

module.exports = coordinaciones;