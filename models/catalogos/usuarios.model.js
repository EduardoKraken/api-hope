const { result } = require("lodash");
const sqlERP     = require("../db.js");
const md5        = require('md5');


//const constructor
const usuarios = (usuarios) => {};

// Agregar datos usuario
usuarios.addUsuario = ( c ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`INSERT INTO usuarios( nombres, apellido_paterno, apellido_materno, edad, sexo, telefono, correo, password, idcoordinaciones, idniveles, foto ) 
      VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )`,
      [ c.nombres, c.apellido_paterno, c.apellido_materno, c.edad, c.sexo, c.telefono, c.correo, md5( c.password ), c.idcoordinaciones, c.idniveles, c.foto ], (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      // RESPUESTA AL CONTROLADOR
      return resolve({ id: res.insertId, ...c });
    });
  })
};

usuarios.getUsuarios = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT u.*,
      CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS nombre_completo, 
       c.coordinacion, c.color, n.nivel FROM usuarios u
      LEFT JOIN niveles n ON n.idniveles = u.idniveles
      LEFT  JOIN coordinaciones c ON c.idcoordinaciones = u.idcoordinaciones
      WHERE  u.deleted = 0 ;
      `, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

usuarios.getPacientes = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT u.*,
      CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS nombre_completo, 
       c.coordinacion, c.color, n.nivel FROM usuarios u
      LEFT JOIN niveles n ON n.idniveles = u.idniveles
      LEFT  JOIN coordinaciones c ON c.idcoordinaciones = u.idcoordinaciones
      WHERE  u.deleted = 0  AND u.idniveles = 3;
      `, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

usuarios.getTerapeutas = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT u.*,
      CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS nombre_completo, 
       c.coordinacion, c.color, n.nivel FROM usuarios u
      LEFT JOIN niveles n ON n.idniveles = u.idniveles
      LEFT  JOIN coordinaciones c ON c.idcoordinaciones = u.idcoordinaciones
      WHERE  u.deleted = 0 AND u.idniveles = 2;
      `, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

usuarios.updateUsuario = ( c, id ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(` UPDATE usuarios SET nombres = ?, apellido_paterno = ?, apellido_materno = ?, edad = ?, sexo = ?, telefono = ?,
      correo = ?, password = ?, idcoordinaciones = ?, foto = ?, estatus = ?, deleted = ?, idniveles = ? WHERE idusuarios = ?`, 
      [ c.nombres, c.apellido_paterno, c.apellido_materno, c.edad, c.sexo, c.telefono, c.correo, md5( c.password ), c.idcoordinaciones, c.foto, c.estatus, c.deleted, c.idniveles, id],
      (err, res) => {

      // VALIDAR ERRORES
      if (err) { return reject({ message: err.sqlMessage }); }

      if (res.affectedRows == 0) { return reject({ message: 'DATO NO ENCONTRADO' }) }

      resolve({ id, ...c });
    })
  })
};

usuarios.getUsuariosActivos = ( ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM usuarios WHERE deleted = 0 AND estatus = 1 ORDER BY nombres;`, (err, res) => {

      // VALIDAR ERRORES
      if (err) {
        return reject({ message: err.sqlMessage });
      }

      // RESPUESTA AL CONTROLADOR
      return resolve( res );
    });
  })
};

usuarios.existeUsuario = ( correo ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT * FROM usuarios WHERE correo = ? AND deleted = 0;`, [ correo ], (err, res) => {
      if (err) {
        console.log( err )
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};

usuarios.validarPassword = ( correo, password ) => {
  return new Promise(( resolve, reject )=>{
    sqlERP.query(`SELECT u.idusuarios, u.nombres, u.apellido_paterno, u.apellido_materno, 
      CONCAT(u.nombres, " ", u.apellido_paterno, " ", u.apellido_materno) AS nombre_completo, 
      u.edad, u.sexo, u.telefono, u.correo, u.idniveles, u.password, u.idcoordinaciones, u.foto,
      u.estatus, u.fecha_creacion, n.nivel, c.coordinacion, c.color FROM usuarios u
      LEFT JOIN niveles n ON n.idniveles = u.idniveles
      LEFT  JOIN coordinaciones c ON c.idcoordinaciones = u.idcoordinaciones
      WHERE u.correo = ? AND u.password = ? AND u.deleted = 0 AND u.estatus = 1;`, 
      [ correo, password ], (err, res) => {
      if (err) {
        console.log( err )
        return reject({ message: err.sqlMessage });
      }
      // Retornamos solo un dato
      return resolve( res[0] );
    });
  })
};

module.exports = usuarios;