module.exports = app => {
  const agenda_citas = require('../../controllers/citas/agenda_citas.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los usuarios
  */ 

  app.post("/agenda.citas.add",          agenda_citas.addAgendaCita);
  app.get("/agenda.citas.get",           agenda_citas.getAgendaCitas);
  app.put("/agenda.citas.update/:id",    agenda_citas.updateAgendaCita);
  app.get("/agenda.citas.activas",       agenda_citas.getAgendasCitasActivas);
  app.get("/agenda.citas.usuario/:id",   agenda_citas.getAgendaCitasUsuario);

};