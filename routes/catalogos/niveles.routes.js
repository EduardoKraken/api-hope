module.exports = app => {
  const niveles = require('../../controllers/catalogos/niveles.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los niveles
  */ 

  app.post("/niveles.add",          niveles.addNivel);
  app.get("/niveles.get",           niveles.getNiveles);
  app.put("/niveles.update/:id",    niveles.updateNivel);
  app.get("/niveles.activos",       niveles.getNivelesActivos);
};