module.exports = app => {
  const usuarios = require('../../controllers/catalogos/usuarios.controllers')
  /*
    Todas las rutas aquí creadas estarán dirigidas hacia los usuarios
  */ 

  app.post("/usuarios.add",          usuarios.addUsuario);
  app.post("/usuarios.foto",         usuarios.addFotoUsuario);
  app.get("/usuarios.get",           usuarios.getUsuarios);
  app.get("/usuarios.terapeutas",    usuarios.getTerapeutas);
  app.put("/usuarios.update/:id",    usuarios.updateUsuario);
  app.get("/usuarios.activos",       usuarios.getUsuariosActivos);
  app.post("/usuario.acceso",        usuarios.validarAcceso);
  app.get("/usuarios.pacientes",     usuarios.getPacientes);


};