module.exports = app => {
  require("./usuarios.routes")(app);
  require("./niveles.routes")(app);
  require("./coordinaciones.routes")(app);
};