// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var cors         = require("cors");
const fileUpload = require("express-fileupload");
const fs         = require("fs");
// IMPORTAR EXPRESS
const app        = express();
const config     = require('./config/index');

//Para servidor con ssl
const http  = require("http");
// Archivo de configuracion
const server = http.createServer(app)

const PORT = config.PORT

// 14 niveles
app.use('/imagenes-hope',          express.static('./../../hope-imagenes'));

// IMPORTAR PERMISOS
app.use(cors({ origin: '*' }));
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos

// ----IMPORTAR RUTAS---------------------------------------->
require('./routes/catalogos/index.routes')(app)
require('./routes/citas/index.routes')(app)
// require('./routes/encuestas/encuestas.routes')(app)

// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
server.listen(PORT, () => {
    console.log(`Servidor Corriendo en el Puerto ${PORT}`);
});
